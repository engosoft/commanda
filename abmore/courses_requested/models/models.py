# -*- coding: utf-8 -*-

from odoo import models, fields, api


class courses_requested(models.Model):
    _name = 'courses_requested.courses_requested'
    _description = 'courses_requested.courses_requested'

    name_of_course = fields.Char(required="1")
    course_name = fields.Char()

