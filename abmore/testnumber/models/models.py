# -*- coding: utf-8 -*-

from odoo import models, fields, api,_

class sogenerator(models.Model):
    _inherit="event.event"
    _name="event.event"
    name_seq_number = fields.Char(string='Order Reference event',  required=True,readonly=True, copy=False, index=True,
                             default=lambda self: _('New'))
    number=fields.Char("Order Reference For Event")
    @api.model
    def create(self, vals):
        if vals.get('name_seq_number', _('New')) == _('New'):
            seq_date = None
            if 'date_order' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['date_order']))
            if 'company_id' in vals:
                vals['name_seq_number'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code(
                    'test.number', sequence_date=seq_date) or _('New')
            else:
                vals['name_seq_number'] = self.env['ir.sequence'].next_by_code('test.number', sequence_date=seq_date) or _('Nehhhhw')

        result = super(sogenerator, self).create(vals)
        return result

#
#
# class diplomainherit(models.Model):
#     _inherit="event.event"
#     _name="event.event"
#
#
# dip_name=fields.Many2many('Diplomat.Diplomat',string="diploma")


class testnumber(models.Model):
    _name = 'testnumber.testnumber'
    _description = 'testnumber.testnumber'

    name = fields.Char()
    value = fields.Integer()
    value2 = fields.Float(compute="_value_pc", store=True)
    description = fields.Text()
    # name_seq_test = fields.Char(string='Order Reference', required=True,readonly=True, copy=False, index=True,
    #                          default=lambda self: _('New'))

    @api.depends('value')
    def _value_pc(self):
        for record in self:
            record.value2 = float(record.value) / 100
    # @api.model
    # def create(self, vals):
    #     if vals.get('name_seq_test', _('New')) == _('New'):
    #         seq_date = None
    #         if 'date_order' in vals:
    #             seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['date_order']))
    #         if 'company_id' in vals:
    #             vals['name_seq_test'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code(
    #                 'test.number', sequence_date=seq_date) or _('New')
    #         else:
    #             vals['name_seq_test'] = self.env['ir.sequence'].next_by_code('test.number', sequence_date=seq_date) or _('Nehhhhw')
    #
    #     result = super(testnumber, self).create(vals)
    #     return result