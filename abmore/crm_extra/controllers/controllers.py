# -*- coding: utf-8 -*-
import base64

from odoo import http
from odoo.http import request, route


class CrmExtra(http.Controller):
    @http.route('/crm_extra/crm_extra/', auth='public')
    def index(self, **kw):
        return "Hello, world"

    @http.route('/crm_extra/crm_extra/objects/', auth='public')
    def list(self, **kw):
        return http.request.render('crm_extra.listing', {
            'root': '/crm_extra/crm_extra',
            'objects': http.request.env['crm_extra.crm_extra'].search([]),
        })

    @http.route('/crm_extra/crm_extra/objects/<model("crm_extra.crm_extra"):obj>/', auth='public')
    def object(self, obj, **kw):
        return http.request.render('crm_extra.object', {
            'object': obj
        })



# @route(['/my/account'], type='http', auth='user', website=True)
# def account(self, redirect=None, **post):
#     partner = request.env.user.partner_id
#     Attachments = request.env['ir.attachment']
#     name = post.get('attachment').filename
#     file = post.get('attachment')
#     attachment_id = Attachments.create({
#         'name': name,
#         'type': 'binary',
#         'datas': base64.b64encode(file.read()),
#         'res_model': partner._name,
#         'res_id': partner.id
#     })
#     partner.update({
#         'attachment': [(4, attachment_id.id)],
#     })