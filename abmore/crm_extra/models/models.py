#-*- coding: utf-8 -*-
from AptUrl.Helpers import _

from addons.website import tools
from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.osv.osv import osv


class contacttolead(models.Model):
    _inherit = 'crm.lead'
    _name = 'crm.lead'
    # location_manychat = fields.Char()
    mobile_app = fields.Char()
    phone = fields.Char(required="1")
    email_from = fields.Char('Email',required="1" ,help="Email address of the contact", tracking=40, index=True)
    description = fields.Text(required="1")
    contact_name = fields.Char('Contact Name', required="1",tracking=30)
    test = fields.Char()
    src_page_manychat = fields.Selection(selection=[('engosoft_saud', 'EngoSoft Saudi '),('engosoft_egypt', 'EngoSoft Egypt'),('engosoft_mech', 'Engosoft Mechanical '),('engosoft_electrical', ' EngoSoft Electrical'),('engosoft_civil', 'Engosoft Civil '),('engosoft_arch', 'Engosoft Arch'),('Engosoft_arab_mech', 'ميكانيكا عرب'),('engosoft_kuwait', 'Engosoft Kwait'), ('engosoft_emiartes', 'Engosoft Emirates')], required="1", string='Source Page')
    lead_source = fields.Selection(selection=[('facebookpage', 'Facebook Page'),('phonecall', 'Phone Call'),('website', 'Website'),('visit', 'visit'),('linkedin', 'linkedin'),('twitter', 'Twitter'),('whataspp', 'whatssapp'),('Event', 'Event'),('Campaign', 'Campaign'),('youtube', 'youtube'),('fb_groups', 'Facebook Groups'),('sa_council', 'council'),('other', 'other')], required="1",string='Lead Source')
    calssifcation_Of_lead = fields.Selection(selection=[('hotlead', 'Hot lead'),('coldlead', 'Cold Lead'),('intermediateelead', 'Intermediate Lead')], string=' lead Classification ')
    no_answer = fields.Selection(selection=[('noanswer', 'No answer'),('answer', 'Answer')], string='Answer')
    lead_type = fields.Selection(selection=[('new_client', 'New Client'),('old_client', 'Old Client'),('company', 'company'),('job', 'job'),('other', 'other')], string='Lead Type')
    # requested_course = fields.Selection(selection=[('infrastructure_dip', 'Infrastructure diploma'),('Leed_ga', 'Leed ga'),('automative_course', 'automotive course'),('fundmental_engi', 'fundamental engineer'),('piping', 'piping engineering'),('bridges', 'bridges كباري'),('Substations', 'Substations'),('hydraulic', 'hydraulic'),('Revit', 'Revit'),('Revit_arch_adv', 'Revit Arch Adv.'),('Revit_str', 'Revit Str. Basic'),('Revit_MEP_basic', 'Revit MEP basic'),('Revit_mep_adv', 'Revit MEP adv.'),('NAvisworkks', 'NAvisworkks'),('Civil 3D', 'Civil 3D'),('PMP', 'PMP'),('Primavera', 'Primavera'),('Power', 'Power'),('low_current', 'low current'),('HVAC', 'HVAC'),('fire', 'fire'),('plumbing', 'plumbing'),('SAP', 'SAP'),('safe', 'safe'),('etabs', 'etabs'),('technical_office_str', 'technical office str'),('technical_office_arch', 'technical office str'),('tech_office_mec', 'tech.office mech'),('tech_office_elec', 'tech.office elec'),('str_design_dip', 'str design dip.'),('arch_design_dip.', 'arch.design dip.'),('mech_design_dip.', 'mech.design dip.'),('elec_design_dip.', 'elec.design dip.'),('دورة_التشطيبات_المعمارية', 'دورة التشطيبات المعمارية'),('photoshop.', 'photoshop'),('illustartor.', 'illustartor'),('indesign.', 'indesign'),('after_effect', 'after effect'),('flash', 'flash'),('BIM_STR_DIP', 'BIM structure diploma'),('BIM_arch_diploma', 'BIM arch diploma'),('BIM_diploma', 'BIM diploma'),('3D-MAX', '3D max'),('lumion', 'lumion'),('PLC1', 'PLC1'),('PLC2', 'PLC2'),('Android', 'Android'),('Java', 'Java'),('Tekla', 'Tekla'),('Site_MANG', 'Site Manager Course '),('Site_MANG', 'Site Manager Course '),('WaterCad', 'WaterCAD'),('SewarCAD', 'SewarCAD'),('StormCAD', 'StormCAD'),('CNC', 'CNC'),('Solidworks', 'Solidworks'),('Excel_Documentation', 'Excel & Documentation'),('advance_Concrete', 'advance Concrete'),('tech_offi_arch_mat', 'tech office arch material '),('elec_online_vid', 'electrical online videos '),('Elevators', 'Elevatrors'),('SewarCAD', 'SewarCAD'),('fire_alarmm', 'Fire Alarm'),('Patron', 'Patron'),('Micatronics', 'Micatronics  ميكاترونكس'),('water_treatment', 'water treatment stations محطات معالجة المياه'),('solar_sys', 'solar systems انظمة الطاقه الشمسية'),('steel_dip', 'steel diploma'),('GIS', 'GIS'),('hydrology', 'hydrology'),('Road_consc', 'Road Construction'),('infrastructure', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS')],required="1", string='Requested Course')
    department = fields.Selection(selection=[('civil', 'Civil '),('arch', 'Architecture'),('structure', 'Structure '),('electrical', 'Electrical'),('mech', 'Mechanical '),('infra', 'infrastructure'),('management', 'Management')],  required="1", string='Department')
    campaign_name=fields.Many2many('campaign.campaign', required="1",string="Campaign Name")
    course_of_engo=fields.Many2many('courses_requested.courses_requested', required="1" ,string="Requested Course Name")
    wrong_num=fields.Boolean('Wrong Number')

    # requested_course_name=fields.Many2many('Courses.Courses',string=" Requsted Courses")
    # event=fields.Many2many('event.event', required="1",string="event Name")
    # char_requested_course = fields.Char()
    # specialism_manychat = fields.Char()
    # service_type=fields.Many2many('servisetype.servisetype',string="Service Type")
    # client_background_manychat = fields.Char()
    # lead_interset_manychat = fields.Char()
    # lead_status = fields.Selection(selection=[('pending', 'Pending'),('contacted', 'Contacted')], string='Lead Status')
    @api.onchange('email_from')
    def get_email_form(self):
        if self.phone:
            email_form_lead = self.env['crm.lead'].search([('email_from', 'ilike', self.email_from)], limit=1)
            if email_form_lead:
                return {

                    'warning':
                        {

                            'title': 'Warning!',

                            'message': ' Email Existing '
                        }

                }

    @api.onchange('phone')
    def get_lead_phone(self):
        if self.phone:
            leads = self.env['crm.lead'].search([('phone', 'ilike', self.phone)], limit=1)
            if leads:
                return {

                    'warning': {

                        'title': 'Warning!',

                        'message': 'Phone Existing'}

                }



    def _create_lead_partner_data(self, name, is_company, parent_id=False):
        """ extract data from lead to create a partner
            :param name : furtur name of the partner
            :param is_company : True if the partner is a company
            :param parent_id : id of the parent partner (False if no parent)
            :returns res.partner record
        """
        res = {
            'name': name,
            'user_id': self.env.context.get('default_user_id') or self.user_id.id,
            'comment': self.description,
            'team_id': self.team_id.id,
            'parent_id': parent_id,
            'phone': self.phone,
            'mobile': self.mobile,
            'email': self.email_from,
            'title': self.title.id,
            'function': self.function,
            'street': self.street,
            'street2': self.street2,
            'zip': self.zip,
            'mobile_app': self.mobile_app,
            'city': self.city,
            'country_id': self.country_id.id,
            # 'location_manychat': self.location_manychat,
            'description': self.description,
            'course_of_engo': self.course_of_engo,
            # 'lead_type': self.lead_type,
            'src_page_manychat': self.src_page_manychat,
            'department': self.department,
            'campaign_name': self.campaign_name,
            'lead_source': self.lead_source,
            # 'no_answer': self.no_answer,
            # 'calssifcation_Of_lead': self.calssifcation_Of_lead,
            'state_id': self.state_id.id,
            'website': self.website,
            'is_company': is_company,
            # 'specialism_manychat': self.specialism_manychat,
            # 'client_background_manychat': self.client_background_manychat,
            # 'lead_interset_manychat': self.lead_interset_manychat,
            'type': 'contact'
        }
        if self.lang_id:
            res['lang'] = self.lang_id.code
        return res


    def _onchange_partner_id_values(self, partner_id):
        """ returns the new values when partner_id has changed """
        if partner_id:
            partner = self.env['res.partner'].browse(partner_id)

            partner_name = partner.parent_id.name
            if not partner_name and partner.is_company:
                partner_name = partner.name

            return {
                'partner_name': partner_name,
                'contact_name': partner.name if not partner.is_company else False,
                'title': partner.title.id,
                'street': partner.street,
                'street2': partner.street2,
                'city': partner.city,
                'state_id': partner.state_id.id,
                'country_id': partner.country_id.id,
                'email_from': partner.email,
                'phone': partner.phone,
                'mobile': partner.mobile,
                'zip': partner.zip,
                'function': partner.function,
                'website': partner.website,
                'campaign_name': partner.campaign_name,
                'course_of_engo': partner.course_of_engo,
                'department': partner.department,
                'src_page_manychat': partner.src_page_manychat,
                'requested_course': partner.requested_course,
                'description': partner.description,
                'name': partner.name,

            }
        return {}



class crmlead(models.Model):
    _inherit = 'res.partner'
    _name = 'res.partner'
    # location_manychat = fields.Char()
    mobile_app = fields.Char()
    # test = fields.Char()
    description = fields.Text( required="1")
    src_page_manychat = fields.Selection(selection=[('engosoft_saud', 'EngoSoft Saudi '),('engosoft_egypt', 'EngoSoft Egypt'),('engosoft_mech', 'Engosoft Mechanical '),('engosoft_electrical', ' EngoSoft Electrical'),('engosoft_civil', 'Engosoft Civil '),('engosoft_arch', 'Engosoft Arch'),('Engosoft_arab_mech', 'ميكانيكا عرب'),('engosoft_kuwait', 'Engosoft Kwait'), ('engosoft_emiartes', 'Engosoft Emirates')],required="1", string='Source Page')
    # calssifcation_Of_lead = fields.Selection(selection=[('hotlead', 'Hot lead'),('coldlead', 'Cold Lead'),('intermediateelead', 'Intermediate Lead')], string=' lead Classification ')
    # no_answer = fields.Selection(selection=[('noanswer', 'No answer'),('answer', 'Answer')], string='Answer')
    lead_source = fields.Selection(selection=[('facebookpage', 'Facebook Page'),('website', 'Website'),('visit', 'visit'),('linkedin', 'linkedin'),('twitter', 'Twitter'),('whataspp', 'whatssapp'),('Event', 'Event'),('Campaign', 'Campaign'),('youtube', 'youtube'),('fb_groups', 'Facebook Groups'),('sa_council', 'council'),('other', 'other')], string='Lead Source')
    # lead_type = fields.Selection(selection=[('new_client', 'New Client'),('old_client', 'Old Client'),('company', 'company'),('job', 'job'),('other', 'other')], string='Lead Type')
    # requested_course = fields.Selection(selection=[('infrastructure_dip', 'Infrastructure diploma'),('Leed_ga', 'Leed ga'),('automative_course', 'automotive course'),('fundmental_engi', 'fundamental engineer'),('piping', 'piping engineering'),('bridges', 'bridges كباري'),('Substations', 'Substations'),('hydraulic', 'hydraulic'),('Revit', 'Revit'),('Revit_arch_adv', 'Revit Arch Adv.'),('Revit_str', 'Revit Str. Basic'),('Revit_MEP_basic', 'Revit MEP basic'),('Revit_mep_adv', 'Revit MEP adv.'),('NAvisworkks', 'NAvisworkks'),('Civil 3D', 'Civil 3D'),('PMP', 'PMP'),('Primavera', 'Primavera'),('Power', 'Power'),('low_current', 'low current'),('HVAC', 'HVAC'),('fire', 'fire'),('plumbing', 'plumbing'),('SAP', 'SAP'),('safe', 'safe'),('etabs', 'etabs'),('technical_office_str', 'technical office str'),('technical_office_arch', 'technical office str'),('tech_office_mec', 'tech.office mech'),('tech_office_elec', 'tech.office elec'),('str_design_dip', 'str design dip.'),('arch_design_dip.', 'arch.design dip.'),('mech_design_dip.', 'mech.design dip.'),('elec_design_dip.', 'elec.design dip.'),('دورة_التشطيبات_المعمارية', 'دورة التشطيبات المعمارية'),('photoshop.', 'photoshop'),('illustartor.', 'illustartor'),('indesign.', 'indesign'),('after_effect', 'after effect'),('flash', 'flash'),('BIM_STR_DIP', 'BIM structure diploma'),('BIM_arch_diploma', 'BIM arch diploma'),('BIM_diploma', 'BIM diploma'),('3D-MAX', '3D max'),('lumion', 'lumion'),('PLC1', 'PLC1'),('PLC2', 'PLC2'),('Android', 'Android'),('Java', 'Java'),('Tekla', 'Tekla'),('Site_MANG', 'Site Manager Course '),('Site_MANG', 'Site Manager Course '),('WaterCad', 'WaterCAD'),('SewarCAD', 'SewarCAD'),('StormCAD', 'StormCAD'),('CNC', 'CNC'),('Solidworks', 'Solidworks'),('Excel_Documentation', 'Excel & Documentation'),('advance_Concrete', 'advance Concrete'),('tech_offi_arch_mat', 'tech office arch material '),('elec_online_vid', 'electrical online videos '),('Elevators', 'Elevatrors'),('SewarCAD', 'SewarCAD'),('fire_alarmm', 'Fire Alarm'),('Patron', 'Patron'),('Micatronics', 'Micatronics  ميكاترونكس'),('water_treatment', 'water treatment stations محطات معالجة المياه'),('solar_sys', 'solar systems انظمة الطاقه الشمسية'),('steel_dip', 'steel diploma'),('GIS', 'GIS'),('hydrology', 'hydrology'),('Road_consc', 'Road Construction'),('infrastructure', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS')],required="1", string='Requested Course')
    department = fields.Selection(selection=[('civil', 'Civil '),('arch', 'Architecture'),('structure', 'Structure '),('electrical', 'Electrical'),('mech', 'Mechanical '),('infra', 'infrastructure'),('management', 'Management')],required="1", string='Department')
    campaign_name=fields.Many2many('campaign.campaign', required="1",string="Campaign Name")
    course_of_engo=fields.Many2many('courses_requested.courses_requested', required="1",string="Requsted Course Name")

    # requested_course_name=fields.Many2many('Courses.Courses',string=" Requsted Courses")

    # event=fields.Many2many('event.event', required="1",string="event Name")

    # client_background_manychat = fields.Char()
    # lead_interset_manychat = fields.Char()
    # service_type=fields.Many2many('servisetype.servisetype',string="Service Type")
    # lead_status = fields.Selection(selection=[('pending', 'Pending'),('contacted', 'Contacted')], string='Lead status')
    # char_requested_course = fields.Char()
    # specialism_manychat = fields.Char()



class crm_extra(models.Model):
    _name = 'crm_extra.crm_extra'
    _description = 'crm_extra.crm_extra'

    name = fields.Char()
    value = fields.Integer()
    value2 = fields.Float(compute="_value_pc", store=True)
    description = fields.Text()
    # file = fields.Binary("Attachment")
    # file_name = fields.Char("File Name")



    # datas = fields.Binary('File')
    # filename = fields.Char('File Name')
    @api.depends('value')
    def _value_pc(self):
        for record in self:
            record.value2 = float(record.value) / 100
