#-*- coding: utf-8 -*-

from odoo import models, fields, api


#inherit to crm.lead
class crmlead(models.Model):
    _inherit = 'crm.lead'
    _name = 'crm.lead'
    location_manychat = fields.Char( )
    mobile_app = fields.Char()
    description = fields.Text()
    specialism_manychat = fields.Char()
    src_page_manychat =fields.Selection(selection=[('engosoft_saudi', 'engosoft saudi'),('engosoft_egypt', 'engosoft egypt'),('engosoft_mech', 'Engosoft Mechanical'),('engosoft_elec', 'Engosoft electrical'),('engosoft_civil', 'Engosoft Civil'),('engosoft_arch', 'Engosoft Arch'),('engosoft_kuwait', 'Engosoft Kuwait'),('engosoft_emirates', 'Engosoft Emirates'),('engosoft_arab', ' ميكانيكا عرب')], string='Lead Type')
    client_background_manychat = fields.Char()
    lead_interset_manychat = fields.Char()
    calssifcation_Of_lead = fields.Selection(selection=[('hotlead', 'Hot lead'),('coldlead', 'Cold Lead'),('intermediateelead', 'Intermediate Lead')], string=' lead Classification ')
    no_answer = fields.Selection(selection=[('noanswer', 'No answer'),('answer', 'Answer')], string='Answer')
    lead_type =  fields.Char()
    requested_course = fields.Selection(selection=[('infrastructure_dip', 'Infrastructure diploma'),('Leed_ga', 'Leed ga'),('automative_course', 'automotive course'),('fundmental_engi', 'fundamental engineer'),('piping', 'piping engineering'),('bridges', 'bridges كباري'),('Substations', 'Substations'),('hydraulic', 'hydraulic'),('Revit', 'Revit'),('Revit_arch_adv', 'Revit Arch Adv.'),('Revit_str', 'Revit Str. Basic'),('Revit_MEP_basic', 'Revit MEP basic'),('Revit_mep_adv', 'Revit MEP adv.'),('NAvisworkks', 'NAvisworkks'),('Civil 3D', 'Civil 3D'),('PMP', 'PMP'),('Primavera', 'Primavera'),('Power', 'Power'),('low_current', 'low current'),('HVAC', 'HVAC'),('fire', 'fire'),('plumbing', 'plumbing'),('SAP', 'SAP'),('safe', 'safe'),('etabs', 'etabs'),('technical_office_str', 'technical office str'),('technical_office_arch', 'technical office str'),('tech_office_mec', 'tech.office mech'),('tech_office_elec', 'tech.office elec'),('str_design_dip', 'str design dip.'),('arch_design_dip.', 'arch.design dip.'),('mech_design_dip.', 'mech.design dip.'),('elec_design_dip.', 'elec.design dip.'),('دورة_التشطيبات_المعمارية', 'دورة التشطيبات المعمارية'),('photoshop.', 'photoshop'),('illustartor.', 'illustartor'),('indesign.', 'indesign'),('after_effect', 'after effect'),('flash', 'flash'),('BIM_STR_DIP', 'BIM structure diploma'),('BIM_arch_diploma', 'BIM arch diploma'),('BIM_diploma', 'BIM diploma'),('3D-MAX', '3D max'),('lumion', 'lumion'),('PLC1', 'PLC1'),('PLC2', 'PLC2'),('Android', 'Android'),('Java', 'Java'),('Tekla', 'Tekla'),('Site_MANG', 'Site Manager Course '),('Site_MANG', 'Site Manager Course '),('WaterCad', 'WaterCAD'),('SewarCAD', 'SewarCAD'),('StormCAD', 'StormCAD'),('CNC', 'CNC'),('Solidworks', 'Solidworks'),('Excel_Documentation', 'Excel & Documentation'),('advance_Concrete', 'advance Concrete'),('tech_offi_arch_mat', 'tech office arch material '),('elec_online_vid', 'electrical online videos '),('Elevators', 'Elevatrors'),('SewarCAD', 'SewarCAD'),('fire_alarmm', 'Fire Alarm'),('Patron', 'Patron'),('Micatronics', 'Micatronics  ميكاترونكس'),('water_treatment', 'water treatment stations محطات معالجة المياه'),('solar_sys', 'solar systems انظمة الطاقه الشمسية'),('steel_dip', 'steel diploma'),('GIS', 'GIS'),('hydrology', 'hydrology'),('Road_consc', 'Road Construction'),('infrastructure', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS')], string='Requested Course')
    department =  fields.Char()
    camname2=fields.Many2many('campaign.campaign',string="Campaign Name")
    servisetype=fields.Many2many('servisetype.servisetype',string="Service Type ")
    char_requseted_course = fields.Char()



class crm_updates(models.Model):
    _name = 'crm_updates.crm_updates'
    _description = 'crm_updates.crm_updates'
    location_manychat = fields.Char( )
    specialism_manychat = fields.Char()
    src_page_manychat =fields.Selection(selection=[('engosoft_saudi', 'engosoft saudi'),('engosoft_egypt', 'engosoft egypt'),('engosoft_mech', 'Engosoft Mechanical'),('engosoft_elec', 'Engosoft electrical'),('engosoft_civil', 'Engosoft civil'),('engosoft_arch', 'Engosoft Arch'),('engosoft_kuwait', 'Engosoft Kuwait'),('engosoft_emirates', 'Engosoft Emirates'),('engosoft_arab', ' ميكانيكا عرب')], string='Lead Type')
    client_background_manychat = fields.Char()
    lead_interset_manychat = fields.Char()
    calssifcation_Of_lead = fields.Selection(selection=[('hotlead', 'Hot lead'),('coldlead', 'Cold Lead'),('intermediateelead', 'Intermediate Lead')], string=' lead Classification ')
    no_answer = fields.Selection(selection=[('noanswer', 'No answer'),('answer', 'Answer')], string='Answer')
    lead_type = fields.Selection(selection=[('new_client', 'New Client'),('old_client', 'Old Client'),('company', 'comapny'),('job', 'Job')], string='Lead Type')
    requested_course = fields.Selection(selection=[('Revit', 'Revit'),('Revit_arch_adv', 'Revit Arch Adv.'),('Revit_str', 'Revit Str. Basic'),('Revit_MEP_basic', 'Revit MEP basic'),('Revit_mep_adv', 'Revit MEP adv.'),('NAvisworkks', 'NAvisworkks'),('Civil 3D', 'Civil 3D'),('PMP', 'PMP'),('Primavera', 'Primavera'),('Power', 'Power'),('low_current', 'low current'),('HVAC', 'HVAC'),('fire', 'fire'),('plumbing', 'plumbing'),('SAP', 'SAP'),('safe', 'safe'),('etabs', 'etabs'),('technical_office_str', 'technical office str'),('technical_office_arch', 'technical office str'),('tech_office_mec', 'tech.office mech'),('tech_office_elec', 'tech.office elec'),('str_design_dip', 'str design dip.'),('arch_design_dip.', 'arch.design dip.'),('mech_design_dip.', 'mech.design dip.'),('elec_design_dip.', 'elec.design dip.'),('دورة_التشطيبات_المعمارية', 'دورة التشطيبات المعمارية'),('photoshop.', 'photoshop'),('illustartor.', 'illustartor'),('indesign.', 'indesign'),('after_effect', 'after effect'),('flash', 'flash'),('BIM_STR_DIP', 'BIM structure diploma'),('BIM_arch_diploma', 'BIM arch diploma'),('BIM_diploma', 'BIM diploma'),('3D-MAX', '3D max'),('lumion', 'lumion'),('PLC1', 'PLC1'),('PLC2', 'PLC2'),('Android', 'Android'),('Java', 'Java'),('Tekla', 'Tekla'),('Site_MANG', 'Site Manager Course '),('Site_MANG', 'Site Manager Course '),('WaterCad', 'WaterCAD'),('SewarCAD', 'SewarCAD'),('StormCAD', 'StormCAD'),('CNC', 'CNC'),('Solidworks', 'Solidworks'),('Excel_Documentation', 'Excel & Documentation'),('advance_Concrete', 'advance Concrete'),('tech_offi_arch_mat', 'tech office arch material '),('elec_online_vid', 'electrical online videos '),('Elevators', 'Elevatrors'),('SewarCAD', 'SewarCAD'),('fire_alarmm', 'Fire Alarm'),('Patron', 'Patron'),('Micatronics', 'Micatronics  ميكاترونكس'),('water_treatment', 'water treatment stations محطات معالجة المياه'),('solar_sys', 'solar systems انظمة الطاقه الشمسية'),('steel_dip', 'steel diploma'),('GIS', 'GIS'),('hydrology', 'hydrology'),('Road_consc', 'Road Construction'),('infrastructure', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS'),('GIS', 'GIS')], string='Requested Course')
    department = fields.Selection(selection=[('civil', 'Civil'),('electrical', 'Electical'),('structure', 'Structure'),('mechanical', 'Mechanical'),('managment', 'Managment')], string='Department')
    camname2=fields.Many2many('campaign.campaign',string="Campaign Name")
    servisetype=fields.Many2many('servisetype.servisetype',string="Service Type ")

