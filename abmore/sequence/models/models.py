# -*- coding: utf-8 -*-

from odoo import models,_,fields, api


class sequence(models.Model):
    _name = 'sequence.sequence'
    _description = 'sequence.sequence'

    name = fields.Char()
    value = fields.Integer()
    value2 = fields.Float(compute="_value_pc", store=True)
    description = fields.Text()
    name_seq = fields.Char(string='Order Reference', required=True, copy=False, readonly=True, index=True, default=lambda self: _('New'))


    @api.depends('value')
    def _value_pc(self):
        for record in self:
            record.value2 = float(record.value) / 100



    @api.model
    def create(self, vals):
        if vals.get('name_seq', _('New')) == _('New'):
            seq_date = None
            if 'date_order' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['date_order']))
            if 'company_id' in vals:
                vals['name'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code(
                    'sale.order', sequence_date=seq_date) or _('New')
            else:
                vals['name_seq'] = self.env['ir.sequence'].next_by_code('sequence.sequence.value', sequence_date=seq_date) or _('Nehhhhw')

        result = super(sequence, self).create(vals)
        return result