# -*- coding: utf-8 -*-

from odoo import models, fields, api,_

class diploaa(models.Model):
    _inherit = 'event.event'
    _name = 'event.event'

    diploma=fields.Many2many('diplomat.diplomat',required="1",string="diplomaaaa Name")




class diplomat(models.Model):
    _name = 'diplomat.diplomat'
    _description = 'diplomat.diplomat'

    diploma_name = fields.Char( required="1")
    begin_date = fields.Datetime( required="1")
    dip_seq_number = fields.Char(string='Order Reference', required=True,readonly=True, copy=False, index=True,
                             default=lambda self: _('New'))


    @api.model
    def create(self, vals):
        if vals.get('dip_seq_number', _('New')) == _('New'):
            seq_date = None
            if 'date_order' in vals:
                seq_date = fields.Datetime.context_timestamp(self, fields.Datetime.to_datetime(vals['date_order']))
            if 'company_id' in vals:
                vals['dip_seq_number'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code(
                    'diplomat.number', sequence_date=seq_date) or _('New')
            else:
                vals['dip_seq_number'] = self.env['ir.sequence'].next_by_code('diplomat.number', sequence_date=seq_date) or _('Nehw')

        result = super(diplomat, self).create(vals)
        return result

#
