# -*- coding: utf-8 -*-

from odoo import models, fields, api


class servisetype(models.Model):
    _name = 'servisetype.servisetype'
    _description = 'servisetype.servisetype'

    name = fields.Selection(selection=[('ofline', 'oflline'),('online', 'online'),('videos', 'videos'),('private', 'private')], string='Requested service')


