#-*- coding: utf-8 -*-

from odoo import models, fields, api

class camtest(models.Model):
    _inherit = 'campaign.campaign'
    _name = 'campaign.campaign'
    account_number = fields.Selection(selection=[('AC!1', 'Account Number 1'),('AC!2', 'Account Number 1'),('AC3', 'Account Number 2')], string='Accounts Number')
    # payment_type   = fields.Selection(selection=[('visa', 'Visa'),('vodafone_cache', 'vodafone Cache'),('paybal', 'Paybal')], string='Payments Type')

   # @api.depends('value')
    # def _value_pc(self):
    #     for record in self:
    #         record.value2 = float(record.value) / 100



class adsaccount(models.Model):
    _name = 'adsaccount.adsaccount'
    _description   = 'adsaccount.adsaccount'
    account_number = fields.Selection(selection=[('AC!1', 'Account Number 1'),('AC!2', 'Account Number 1'),('AC3', 'Account Number 2')], string='Accounts Number')
    payment_type   = fields.Selection(selection=[('visa', 'Visa'),('vodafone_cache', 'vodafone Cache'),('paybal', 'Paybal')], string='Payments Type')
    payments = fields.Float()
    status = fields.Selection(selection=[('suspend', 'Suspended'),('using', 'Still Uing')], string='Status')

    # @api.depends('value')
    # def _value_pc(self):
    #     for record in self:
    #         record.value2 = float(record.value) / 100
