#-*- coding: utf-8 -*-

from psycopg2 import sql

from odoo import models, fields, api, exceptions
from psycopg2 import sql

#
# class camst(models.Model):
#     _inherit = 'crm.lead'
#     _name = 'crm.lead'
#     name = fields.Many2many('campaign.campaign')

from odoo.addons.test_convert.tests.test_env import record
from odoo.cli.scaffold import env





class campaign(models.Model):
    _name = 'campaign.campaign'
    _description = 'campaign.campaign'
    name = fields.Char(required=True, string="Campaign")
    camnamet=fields.Many2many('mailing.mailing',string="Campaign Maill & SMS")
    event_name=fields.Many2many('event.event','name',required=True,string="events")
    # camname = fields.Many2one('crm.lead', string="Campssaign")
    x = fields.Many2many('res.country', string="country")
    channel = fields.Selection(selection=[('Google_ads', 'Google ads'),('instgram', 'Instgram'),('facebook', 'Facebook'),('youtube', 'Youtube'),('twitter', 'Twitter'),('linkedin', 'Linkedin'),('sms', 'SMS'),('other', 'Other'),], string='Channel', required=True)
    audiance = fields.Selection(selection=[('mechnical', 'Mechnical'),('electrical', 'Electrical'),('civil', 'Civil'),('arch', 'ARCH'),('management', 'Management'),('health_safety', 'Health and saftey '),], string='Audience',  required="1")
    budjet_type = fields.Selection(selection=[('lifetime', 'Life Time Budjet'),('daily', 'Daily budjet'),('other', 'Other'),], string='Budjet Type',  required="1")
    startdate = fields.Datetime( required="1")
    enddate = fields.Datetime( )
    # actual_end_date = fields.Datetime( required="1", default=lambda self: fields.datetime.now())
    actual_spent_budjet = fields.Integer()
    # ads_account_number = fields.Integer()
    ads_link = fields.Char(required="1")
    status =  fields.Selection(selection=[('pland', 'planned'),('ongoing', ' On Going'),('Stopped', 'Stopped'),('finshed', 'Finshed'),], string='Status ', default='ongoing')
    accounts_facebook =  fields.Selection(selection=[('FP_iskander', 'Account Iskander'),('FP_ahmed_wagdy', ' Account Ahmed Wagdy'),('FP_MECH_ARAB', 'Account Mech Arab'),], string='Facebook Accounts ')
    pages_facebook_iskander =  fields.Selection(selection=[('Page1', 'Page 1 Iskander'),('page2', 'Page 2 Iskander'),('Page3', 'Page 3 Iskander'),], string='Pages of Isakander Account')
    pages_facebook_wagdy =  fields.Selection(selection=[('Page_1', 'Page 1 wagdy'),('page_2', 'Page 2 wagdy'),('Page_3', 'Page 3 wagdy'),], string='Pages of  Ahmed Wagdy Account')
    pages_facebook_mech_arab =  fields.Selection(selection=[('Page_11', 'Page 1 Mech Arab'),('page_22', 'Page 2  Mech Arab'),('Page_3', 'Page 3  Mech Arab'),], string='Pages of   Mechincal Arab Account')
    accounts_google =  fields.Selection(selection=[('AC1', ' Google Account 1'),('AC2', ' Google Account 2'),('AC3', 'Google Account 3'),], string='Google Accounts ')
    accounts_linkedin =  fields.Selection(selection=[('LC1', ' linkedin Account 1'),('LC2', ' linkedin Account 2'),('AC3', 'linkedin Account 3'),], string='linkedin Accounts ')
    accounts_instgram =  fields.Selection(selection=[('IC1', ' Instgram Account 1'),('LC2', ' Instgram Account 2'),('LC3', 'Instgram Account 3'),], string='Instgram Accounts ')
    sms = fields.Selection(selection=[('sms1', ' Sms number 1'),('sms2', 'Sms number 3'),('sms3', 'Sms number3'),], string='Sms Numbers ')
    youtube = fields.Selection(selection=[('channel1', ' Channel 1'),('channel2', 'Channel 2'),('channel 3', 'Channel 3'),], string='Youtube Channels ')
    twiter =  fields.Selection(selection=[('TC1', ' Twiter Account 1'),('TC2', ' Twiter Account 2'),('TC3', 'Twiter Account 3'),], string='Twiter Accounts ')
    notes = fields.Text()
    application_no = fields.Char(string="Campaign Number")

    # ss = fields.Many2many('sms.sms.partner_id' ,string="TEST" )

    # @api.depends('value')
    # def _value_pc(self):
    #     for record in self:
    #         record.value2 = float(record.value) / 100
# class test_test(models.Model):
#     _name = ""

