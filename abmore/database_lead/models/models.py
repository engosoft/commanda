# -*- coding: utf-8 -*-

from odoo import models, fields, api


class database_lead(models.Model):
    _name = 'database_lead.database_lead'
    _description = 'database_lead.database_lead'


    campaign_name=fields.Many2many('campaign.campaign', required="1",string="Campaign Name")
    user_id = fields.Many2many('res.users', string='Salesperson')
    # course_of_engo=fields.Many2many('courses_requested.courses_requested', required="1",string="Requested Course Name")
    name = fields.Char()
    contact_name = fields.Char()
    value = fields.Integer()
    value2 = fields.Float(compute="_value_pc", store=True)
    description = fields.Text()
    src_page_manychat = fields.Selection(
        selection=[('engosoft_saud', 'EngoSoft Saudi '), ('engosoft_egypt', 'EngoSoft Egypt'),
                   ('engosoft_mech', 'Engosoft Mechanical '), ('engosoft_electrical', ' EngoSoft Electrical'),
                   ('engosoft_civil', 'Engosoft Civil '), ('engosoft_arch', 'Engosoft Arch'),
                   ('Engosoft_arab_mech', 'ميكانيكا عرب'), ('engosoft_kuwait', 'Engosoft Kwait'),
                   ('engosoft_emiartes', 'Engosoft Emirates')], required="1", string='Source Page')

    lead_source = fields.Selection(
        selection=[('facebookpage', 'Facebook Page'), ('website', 'Website'), ('visit', 'visit'),
                   ('linkedin', 'linkedin'), ('twitter', 'Twitter'), ('whataspp', 'whatssapp'), ('Event', 'Event'),
                   ('Campaign', 'Campaign'), ('youtube', 'youtube'), ('fb_groups', 'Facebook Groups'),
                   ('sa_council', 'council'), ('other', 'other')], string='Lead Source')

    calssifcation_Of_lead = fields.Selection(
        selection=[('hotlead', 'Hot lead'), ('coldlead', 'Cold Lead'), ('intermediateelead', 'Intermediate Lead')],
        string=' lead Classification ')

    no_answer = fields.Selection(selection=[('noanswer', 'No answer'), ('answer', 'Answer')], string='Answer')
    lead_type = fields.Selection(
        selection=[('new_client', 'New Client'), ('old_client', 'Old Client'), ('company', 'company'), ('job', 'job'),
                   ('other', 'other')], string='Lead Type')

    department = fields.Selection(selection=[('civil', 'Civil '), ('arch', 'Architecture'), ('structure', 'Structure '),
                                             ('electrical', 'Electrical'), ('mech', 'Mechanical '),
                                             ('infra', 'infrastructure'), ('management', 'Management')], required="1",
                                  string='Department')
    wrong_num=fields.Boolean('Wrong Number')
    email_from = fields.Char('Email',required="1" ,help="Email address of the contact", tracking=40, index=True)
    country_id = fields.Many2many('res.country', string='Country')


    @api.depends('value')
    def _value_pc(self):
        for record in self:
            record.value2 = float(record.value) / 100
